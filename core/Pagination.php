<?php
/**
 * Author: Klyachenkov Gennady
 * Version: 1.0
 * Date: 31.03.2016
 */

class Pagination{

    protected $config = [];
    protected $defaultConfig = [
        'total'=>0,
        'currentPage'=>0,
        'totalPage'=>1,
        'countPerPage'=>10,
        'query'=>[],
        'target'=>'',

        'usePrevLink'=>true,
        'prevLinkName'=>'&laquo;',
        'prevLinkNum'=>1,
        'useNextLink'=>true,
        'nextLinkName'=>'&raquo;',
        'nexLinkNum'=>1,

        'activeClassName'=>'active',
        'prevClassName'=>'prev',
        'nextClassName'=>'next',
        'paginationClassName'=>'pagination',
    ];

    protected $template = '<ul class="{{paginationClassName}}">{{prevLink}}{{list}}{{nextLink}}</ul>';
    protected $linkTemplate = '<li class="{{active}} {{class}}"><a href="{{queryString}}">{{num}}</a></li>';

    public function __get($name)
    {
        if(isset($this->config[$name])){
            return $this->config[$name];
        }
        if(method_exists($this, $name)){
            return $this->$name();
        }
        if(method_exists($this, 'get' . ucfirst($name))){
            return $this->{'get' . ucfirst($name)}();
        }
        return null;
    }

    public function __set($name, $value)
    {
        if(isset($this->config[$name])){
            $this->config[$name] = $value;
        }
    }

    public function __construct($params = [])
    {
        if($params){
            $this->fill($params);
        }
    }

    protected function fill($params)
    {
        $this->config = array_merge($this->defaultConfig, $params);
        if(!$this->currentPage){
            $this->currentPage = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        }
        if($this->currentPage > 1) {
            $this->prevLinkNum = $this->currentPage - 1;
        }
        if($this->total > $this->countPerPage){
            $this->totalPage = ceil($this->total / $this->countPerPage);
        }
        if($this->totalPage > 1){
            $this->nextLinkNum = $this->currentPage < $this->totalPage ? $this->currentPage + 1 : $this->totalPage;
        }
    }

    public function render($params = [])
    {
        if($params){
            $this->fill($params);
        }
        if($this->totalPage > 1) {
            return Render::string($this->template, $this);
        }
        return '';
    }

    public function getList()
    {
        $list = '';

        for($i=1; $i<=$this->totalPage; $i++){
            $page = new stdClass();
            $page->num = $i;
            $page->queryString = '?' . $this->getQueryString(['page'=>$i]);
            $page->active = $i == $this->currentPage ? $this->activeClassName:'';
            $page->class = '';
            $list.= Render::string($this->linkTemplate, $page);
        }

        return $list;
    }

    public function getPrevLink()
    {
        if($this->usePrevLink && $this->prevLinkNum != $this->currentPage){
            $page = new stdClass();
            $page->num = $this->prevLinkName;
            $page->active = '';
            $page->class = $this->prevClassName;
            $page->queryString = '?' . $this->getQueryString(['page'=>$this->prevLinkNum]);

            return Render::string($this->linkTemplate, $page);
        }
        return '';
    }

    public function getNextLink()
    {
        if($this->useNextLink && $this->totalPage != $this->currentPage){
            $page = new stdClass();
            $page->num = $this->nextLinkName;
            $page->active = '';
            $page->class = $this->nextClassName;
            $page->queryString = '?' . $this->getQueryString(['page'=>$this->nextLinkNum]);

            return Render::string($this->linkTemplate, $page);
        }
        return '';
    }

    public function getQueryString($param = [])
    {
        return http_build_query(array_merge($this->query, $param));
    }

}