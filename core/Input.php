<?php
/**
 * Author: Klyachenkov Gennady
 * Date: 24.03.2016
 */
class Input{

    use SingletonTrait;
    /** @var Input $_instance */

    protected $properties = [];
    protected $fillable = [];

    protected function __construct() {
        $this->properties = filter_var_array($_REQUEST);
    }

    /**
     * Определяет, является ли запрос AJAX'овым
     * @return bool
     */
    static public function isAjax()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
        !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
        && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }
}