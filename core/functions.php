<?php
/**
 * Description: просто сборник вспомогательных функций. Подключается в самом начале отработки движка.
 * Author: Klyachenkov Gennady
 * Date: 20.06.2016
 */

/**
 * Возвращает название месяца в виде строки
 * @param $month порядковый номер месяца
 * @param string $case вариант произношения:
 * regular - именительный падеж
 * short - сокращенное название
 * genitive - родительный подаже, полный вариант
 * @return null
 */
function sayMonth($month, $case = 'regular')
{
    $month = intval($month);
    return Config::get('months.' . $case . '.' . $month, '');
}

/**
 * Свой автозагрузчик классов
 * @param $className
 */
function coreAutoload($className)
{
    $filename = CORE . DIRECTORY_SEPARATOR . $className . ".php";
    if (is_readable($filename)) {
        require $filename;
    }
}