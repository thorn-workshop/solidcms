<?php

class Site{
    use SingletonTrait;

    /** @var Site $_instance*/
    protected $properties = [];
    protected $fillable = [
        'article'
    ];

    /**
     * Контроллер обычного вывода страницы
     * @param $id string идентификатор статьи для рендера.
     */
    static public function showArticle($id){
        $article = new Article($id);
        self::set('article', $article);
        echo Render::template($article->get('template'));
    }

    /**
     * Возвращает хешированную (соленую) строку
     * @param string $string
     * @return string
     */
    public static function hash($string='')
    {
        return md5($string . Config::get('system.salt'));
    }

}