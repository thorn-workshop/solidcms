<?php
/**
 * Author: Klyachenkov Gennady
 * Date: 22.03.2016
 */
class Index {

    protected $items;
    /** @var Index */
    private static $_instance = null;

    private function __construct() {
        try {
            $this->items = Data::read('systemIndex');
        } catch(Exception $e){
            $this->items = $this->collectArticles();
            // дополнительно индексируем вложенные статьи
            foreach($this->items as $art){
                if($parent = $this->getItemByKey($art['parent'])){
                    $parent['children'][] = $art['id'];
                    $this->setItemByKey($parent['id'], $parent);
                }
            }
            Data::save('systemIndex', $this->items);
        }
    }
    protected function __clone() {}
    protected function __wakeup() {}

    static public function getInstance() {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    /**
     * Получение статьи из индекса по ключу
     * @param $key
     * @return array|null
     */
    static public function get($key)
    {
        $index = self::getInstance();
        return $index->getItemByKey($key);
    }
    /**
     * Поиск стайте в индексе по параметру
     * @param $key string имя параметра
     * @param $value mixed значение параметра
     * @return array
     */
    static public function find($key, $value)
    {
        $index = self::getInstance();
        return $index->findItemByKey($key, $value);
    }
    /**
     * Получение статей из каталога
     * @param $key String идентификатор каталога
     * @param integer $type 0 - только статьи, 1 - только каталоги, 2 - все подряд
     * @param bool $recursive - включае вложенные каталоги
     * @return array
     */
    static public function lists($key, $type = 0, $recursive = false, $sortName = 'catalog', $sortOrder = 'DESC')
    {
        $index = self::getInstance();
        $result = [];
        if($parent = $index->getItemByKey($key)){
            foreach($parent['children'] as $child){
                $art = $index->getItemByKey($child);
                if(
                    $type == 0 && !$art['catalog'] ||
                    $type == 1 && $art['catalog'] ||
                    $type == 2
                ) {
                    $result[] = $art;
                }
                if($recursive && $art['catalog']){
                    $result = array_merge($result, self::lists($art['id'], $type, $recursive));
                }
            }
        }
        if(count($result) > 1){
            usort($result, function($a, $b)use($sortName){
                return $a[$sortName] > $b[$sortName] ? -1 : 1;
            });
            if($sortOrder != 'ASC'){
                $children = array_reverse($result);
            }
        }
        return $result;
    }

    /**
     * Возвращает список дочерних статей для выбранного каталога
     * @param string $key идентификатор каталога
     * @param string $sortName поле, по которому будут отсортированы статьи
     * @param string $sortOrder порядок сортировки
     * @return array
     */
    static public function children($key, $sortName = 'catalog', $sortOrder = 'DESC')
    {
        $index = self::getInstance();
        return $index->lists($key, 0, false, $sortName, $sortOrder);
    }
    /**
     * Поиск элементов в индексе по параметру с заданным значением
     * @param $key string
     * @param $value mimxed
     * @return array
     */
    public function findItemByKey($key, $value)
    {
        return array_filter($this->items, function($item)use($key, $value){
            return (isset($item[$key]) && $item[$key] == $value);
        });
    }
    /**
     * Получение элемента из индекса по ключу
     * @param $key string
     * @return null|array
     */
    public function getItemByKey($key)
    {
        if(isset($this->items[$key])){
            return $this->items[$key];
        }
        return null;
    }
    /**
     * Кеширование элемента в индексе по ключу
     * @param $key string
     * @param $item array
     */
    protected function setItemByKey($key, $item)
    {
        $this->items[$key] = $item;
    }
    /**
     * индексирование доступных статей в каталоге
     * @param string $path каталог для поиска статей относительно ARTICLES
     * @return array
     */
    protected function collectArticles($path = '')
    {
        if($path){
            $path = $path . DIRECTORY_SEPARATOR;
        }
        $dir = opendir(ARTICLES . DIRECTORY_SEPARATOR . $path);
        $index = [];

        while($file = readdir($dir)) {
            $pathinfo = pathinfo($file);
            if ($pathinfo['extension'] == 'art' && $file !== '..' && $file !== '.') {
                try{
                    $art = Article::read($path . $pathinfo['filename']);
                    $art->set('editdate', date('d.m.Y H:i', filemtime(ARTICLES . DIRECTORY_SEPARATOR . $path . $file)));
                    $index[$art->get('id')] = $art->trim();

                } catch(Exception $e){}
            }
            if($file !== '..' && $file !== '.' && is_dir(ARTICLES . DIRECTORY_SEPARATOR . $path . $file)){
                $index = $index + $this->collectArticles($path . $file);
            }
        }
        closedir($dir);
        return $index;
    }

}