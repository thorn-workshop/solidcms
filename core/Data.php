<?php
/**
 * Author: Klyachenkov Gennady
 * Date: 22.03.2016
 */
class Data {
    /**
     * Метода безопасной десериализации (на случай использования на разных платформах 32 и 64бит)
     * @param $str
     * @return mixed
     */
    static public function unserialize($str){
        $str = preg_replace_callback('/i:([\d]{10,})/iu', function($m){
            return 's:'.strlen($m[1]).':"'.$m[1].'"';
        }, $str);
        return unserialize($str);
    }
    /**
     * Чтение сериализованных данных из файла
     * @param $name string имя файла
     * @return bool|mixed
     */
    static public function read($name){

        $filePath = DATA . DIRECTORY_SEPARATOR . $name . '.data';
        if(!file_exists($filePath)){
            throw new Exception('Файла данных ' . $name . ' не существует.', 1);
        }

        if(@$data = file_get_contents($filePath)){
            if(@$unData = self::unserialize($data)){
                return $unData;
            } else {
                throw new Exception('Не удалось распаковать данные из файла ' . $name, 3);
            }
        } else {
            throw new Exception('Не удалось прочесть файл данных ' . $name, 2);
        }
    }
    /**
     * Сохранение сериализованных данных в файл
     * @param $name string имя файла данных
     * @param $data array данные для сохранения
     * @return bool
     */
    static public function save($name, $data){
        $fileName = DATA . DIRECTORY_SEPARATOR . $name . '.data';
        $filePath = dirname($fileName);
        if(false === file_put_contents(DATA . DIRECTORY_SEPARATOR . $name . '.data', serialize($data))){
            if(!is_dir($filePath)){
                throw new Exception('Директория для записи не найдена. ' . $filePath, 1);
            }
            if(!is_writable($filePath)){
                throw new Exception('Директория не доступна для записи. ' . $filePath, 2);
            }
        }
        return true;
    }
}