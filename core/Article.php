<?php
/**
 * Created by PhpStorm.
 * Author: Klyachenkov Gennady
 * Date: 22.03.2016
 * Time: 12:47
 */
class Article {

    protected $fillable = [
        'id',
        'url',
        'keywords',
        'description',
        'parent',
        'title',
        'images',
        'notation',
        'preview',
        'content',
        'plugins',
        'catalog',
        'template',
        'pubdate',
        'editdate',
        'children',
        'extra'
    ];
    protected $properties = [];
    protected $loaded = false;

    public function __construct($id = null)
    {
        if($id){
            $this->load($id);
        }
    }

    /**
     * Доступ к параметрам статьи
     * @param string $property название параметра
     * @param mixed $default дефолтное значение параметра, если не задано или пустое
     * @return mixed
     * @throws Exception
     */
    public function get($property, $default = '')
    {
        if(!in_array($property, $this->fillable)){
            throw new Exception('Ошибка доступа к неизвестному параметру ' . $property);
        }
        $methodName = 'get' . ucfirst($property) . 'Property';

        if(method_exists($this, $methodName)){
            return $this->$methodName($this->properties[$property], $default);
        }
        if(!isset($this->properties[$property]) || !$this->properties[$property]){
            return $default;
        }
        return $this->properties[$property];
    }

    /**
     * Установка параметра статьи
     * @param string $property название параметра
     * @param mixed $value значение параметра
     * @return Article
     * @throws Exception
     */
    public function set($property, $value)
    {
        if(!in_array($property, $this->fillable)){
            throw new Exception('Ошибка установки неизвестного параметра ' . $property);
        }

        $methodName = 'set' . ucfirst($property) . 'Property';
        if(method_exists($this, $methodName)){
            $this->$methodName($value);
        } else {
            $this->properties[$property] = $value;
        }

        return $this;
    }

    /**
     * Чтение статьи из основного раздела
     * @param string $id идентификатор статьи
     * @param bool $log оповещать об ошибках
     * @return Article
     */
    static public function read($id){

        $fileName = ARTICLES . DIRECTORY_SEPARATOR . $id . '.art';
        try {
            $file = file_get_contents($fileName);
        } catch(Exception $e){
            throw new Exception('Ошибка чтения статьи: ' . $e->getMessage());
        }

        $art = new Article();

        foreach($art->fillable as $var){
            $text = explode('<!-- '.$var.' -->', $file);
            if(count($text) > 1) {
                $art->set($var, trim($text[1]));
            }
        }
        $art->set('id', (string) $id);
        $art->set('parent', $art->get('parent', 'index'));
        $art->set('pubdate', $art->get('pubdate', date(Config::get('system.dateTimeFormat'), filemtime($fileName)) ));
        $art->set('catalog', $art->get('catalog', 0));
        $art->set('keywords', $art->get('keywords', Config::get('system.defaultKeywords')));
        $art->set('description', $art->get('description', Config::get('system.defaultDescription')));

        return $art;
    }

    /**
     * Выжимка из статьи только нужного для индекса
     * @return array
     */
    function trim(){
        $new_art = [
            'id'		=>$this->get('id'),
            'url'		=>$this->get('url'),
            'parent'	=>$this->get('parent'),
            'title' 	=>$this->get('title'),
            'preview' 	=>$this->get('preview'),
            'pubdate' 	=>$this->get('pubdate'),
            'catalog'	=>$this->get('catalog'),
            'extra'		=>$this->get('extra', []),
            'children'	=>$this->get('children', []),
        ];
        return $new_art;
    }

    /**
     * Парсирует и возвращает значение параметра статьи extra, хранимый в ini-формате
     * @param $original
     * @param array $default
     * @return array
     */
    protected function getExtraProperty($original, $default = [])
    {
        try {
            return parse_ini_string($original, true);
        } catch (Exception $e){
            return $default;
        }
    }
    /**
     * Прикрепление extra параметра к статье
     * @param $original
     */
    protected function setExtraProperty($original)
    {
        if(is_array($original)){
            $original = Config::array2ini($original);
        }
        $this->properties['extra'] = $original;
    }

    /**
     * Загрузка и заполнение параметрами статьи по идентификатору
     * @param $id
     * @return bool
     * @throws Exception
     */
    public function load($id)
    {
        $art = $this->read($id);
        if($art){
            $this->fill($art->all());
            $this->loaded = true;
        }
        return $this->loaded;
    }

    /**
     * Массовая устновка параметров статьи. Выбираются только fillable параметры.
     * @param array $properties
     * @return $this
     * @throws Exception
     */
    public function fill($properties = [])
    {
        foreach($properties as $key=>$value){
            try {
                $this->set($key, $value);
            } catch(Exception $e){
                continue;
            }
        }
        return $this;
    }
    /**
     * Возвращает все параметры текущего объекта в виде массива
     * @return array
     * @throws Exception
     */
    public function all()
    {
        $properties = [];
        foreach($this->fillable as $key){
            $properties[$key] = $this->get($key);
        }
        return $properties;
    }

}