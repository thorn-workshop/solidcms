<?php
/**
 * Description:
 * Author: Klyachenkov Gennady
 * Date: 07.07.2016
 */
class User {

    protected $fillable = [
        'login',
        'name',
        'password',
    ];
    protected $properties = [];
    protected $loaded = false;
    protected $auth = false;

    public function __construct($login = null)
    {
        if($login){
            $this->load($login);
        }
    }

    /**
     * Доступ к параметрам пользователя
     * @param string $property название параметра
     * @param mixed $default дефолтное значение параметра, если не задано или пустое
     * @return mixed
     * @throws Exception
     */
    public function get($property, $default = '')
    {
        if(!in_array($property, $this->fillable)){
            throw new Exception('Ошибка доступа к неизвестному параметру ' . $property);
        }
        $methodName = 'get' . ucfirst($property) . 'Property';

        if(method_exists($this, $methodName)){
            return $this->$methodName($this->properties[$property], $default);
        }
        if(!isset($this->properties[$property]) || !$this->properties[$property]){
            return $default;
        }
        return $this->properties[$property];
    }

    /**
     * Установка параметра пользователя
     * @param string $property название параметра
     * @param mixed $value значение параметра
     * @return Article
     * @throws Exception
     */
    public function set($property, $value)
    {
        if(!in_array($property, $this->fillable)){
            throw new Exception('Ошибка установки неизвестного параметра ' . $property);
        }

        $methodName = 'set' . ucfirst($property) . 'Property';
        if(method_exists($this, $methodName)){
            $this->$methodName($value);
        } else {
            $this->properties[$property] = $value;
        }

        return $this;
    }

    /**
     * Чтение данных пользователя по логину
     * @param string $login логин пользователя
     * @return Article
     */
    static public function read($login){

        $user = new self;
        try{
            $userData = Data::read('users' . DIRECTORY_SEPARATOR . $login);
            $user->fill($userData);

        } catch(Exception $e){
            if($e->getCode() === 1){
                throw new Exception('Пользователь ' . $login . ' не существует.', 1);
            }
            throw new Exception('Ошибка чтения данных пользователя ' . $login . '. ' . $e->getMessage(), 2);
        }
        return $user;
    }

    public function save()
    {
        Data::save('users' . DIRECTORY_SEPARATOR . $this->get('login'), $this->all());

        return true;
    }

    /**
     * Загрузка и заполнение параметрами статьи по идентификатору
     * @param $login
     * @return bool
     * @throws Exception
     */
    public function load($login)
    {
        $user = $this->read($login);

        $this->fill($user->all());
        $this->loaded = true;

        return $this->loaded;
    }

    /**
     * Массовая устновка параметров пользователя. Выбираются только fillable параметры.
     * @param array $properties
     * @return $this
     * @throws Exception
     */
    public function fill($properties = [])
    {
        foreach($properties as $key=>$value){
            try {
                $this->set($key, $value);
            } catch(Exception $e){
                continue;
            }
        }
        return $this;
    }
    /**
     * Возвращает все параметры текущего объекта в виде массива
     * @return array
     * @throws Exception
     */
    public function all()
    {
        $properties = [];
        foreach($this->fillable as $key){
            $properties[$key] = $this->get($key);
        }
        return $properties;
    }

    /**
     * Проверка на авторизованность пользователя
     * @param string $pass пароль для авторизации
     * @return bool
     * @throws Exception
     */
    public function auth($pass = null)
    {
        if($pass) {
            $this->auth = $this->get('password') == Site::hash($pass);
        }
        return $this->auth;
    }

}