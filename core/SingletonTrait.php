<?php

trait SingletonTrait{

    private static $_instance = null;
    protected function __construct(){
    }

    protected function __clone() {}
    protected function __wakeup() {}

    static public function getInstance() {
        if(is_null(self::$_instance))
        {
            //$class = get_called_class();
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Получение значения параметра объекта
     * @param $key
     * @param $default
     * @return Article|null
     */
    static public function get($key, $default = '')
    {
        $self = self::getInstance();
        return $self->getProperty($key, $default);
    }

    public function getProperty($key, $default)
    {
        $methodName = 'get' . ucfirst($key) . 'Property';

        if(method_exists($this, $methodName)){
            return $this->$methodName($this->properties[$key], $default);
        }
        if(!isset($this->properties[$key]) || !$this->properties[$key]){
            return $default;
        }
        return $this->properties[$key];
    }
    /**
     * Установка значения параметра объекта
     * @param $key
     * @param $value
     * @throws Exception
     */
    static public function set($key, $value)
    {
        $self = static::getInstance();
        return $self->setProperty($key, $value);
    }

    public function setProperty($key, $value)
    {
        if(!in_array($key, $this->fillable)){
            throw new Exception('Ошибка установки неизвестного параметра ' . $key);
        }

        $methodName = 'set' . ucfirst($key) . 'Property';
        if(method_exists($this, $methodName)){
            $this->$methodName($value);
        } else {
            $this->properties[$key] = $value;
        }

        return $this;
    }

    public function all()
    {
        return $this->properties;
    }

}