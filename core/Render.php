<?php
/**
 * Author: KlyachenkovG
 * Date: 22.03.2016
 */
class Render {
    protected $templates = [];
    protected $plugins = [];
    protected $share = [];
    /** @var Render */
    private static $_instance = null;

    private function __construct() {
    }
    protected function __clone() {}
    protected function __wakeup() {}

    static public function getInstance() {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    static public function template($name, $params = [])
    {
        $render = self::getInstance();
        return $render->renderTemplate($name, $params);
    }

    static public function plugin($name, $params = [])
    {
        $render = self::getInstance();
        return $render->renderPlugin($name, $params);
    }

    static public function share($key, $value)
    {
        $render = self::getInstance();
        return $render->shareVariable($key, $value);
    }

    static public function string($string, $object)
    {
        return preg_replace_callback('/(\{\{(\w+)\}\})/iu', function($matches)use($object){

            $name = null;
            if(count($matches)>2){
                $name = $matches[2];
            }
            return $name ? $object->$name : '';
        }, $string);
    }

    /**
     * исполнение php-кода из контента
     * @param $code контент для выполнения
     * @return string
     */
    protected function evalCode($code, $params){
        $params['params'] = $params;
        extract($params);
        ob_start();
        eval('?>' . $code);
        return ob_get_clean();
    }

    /**
     * Рендер шаблона сайта из папки /template/. Файлы шаблонов кешируются, ускоряя повторный рендер.
     * @param $name string имя шаблона для рендера
     * @param array $params опциональные параметры для рендера, доступные внутри шаблона как $params
     * @return string
     */
    public function renderTemplate($name, $params = []){
        $params = array_merge($this->share, $params);
        if (!isset($this->templates[$name])) {
            try {
                $this->templates[$name] = file_get_contents(RTPL . DIRECTORY_SEPARATOR . $name . '.php');
            } catch (Exception $e){
                exit('Ошибка при чтении файла шаблона: ' . $name . '. ' . $e->getMessage());
            }
        }

        return $this->evalCode($this->templates[$name], $params);
    }

    /**
     * Рендер плагина из /plugins/. Файлы шаблонов кешируются, ускоряя повторный рендер.
     * @param string $name имя плагина для рендера (можно передавать через запятую)
     * @return string
     */
    public function renderPlugin($name, $params = []){
        $names = explode(',', $name);
        $params = array_merge($this->share, $params);
        $filters = isset($params['filter']) ? explode(',', $params['filter']) : [''];
        $echo = '';

        if($names){
            foreach($names as $plugin){
                @list($plugin_name, $plugin_placeholder) = explode(':',trim($plugin));
                if(in_array($plugin_placeholder, $filters)){
                    if (!isset($this->plugins[$plugin_name])) {
                        $this->plugins[$plugin_name] = @file_get_contents(PLUGINS . DIRECTORY_SEPARATOR . $plugin_name . '.php');
                    }
                    $echo.= $this->evalCode($this->plugins[$plugin_name], $params);
                }
            }
        }
        return $echo;
    }

    /**
     * Расшаривание переменной между плагинами и шаблонами.
     * @param $key
     * @param $value
     */
    public function shareVariable($key, $value)
    {
        $this->share[$key] = $value;
    }
}