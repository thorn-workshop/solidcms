<?php
/**
 * Author: Klyachenkov Gennady
 * Date: 22.03.2016
 */
class Config {

    protected $configs = [];
    private static $_instance = null;

    private function __construct() {

    }
    protected function __clone() {}
    protected function __wakeup() {}

    static public function getInstance() {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Получает параметр конфига, используя dot-нотацию
     * @param $name
     * @return null
     */
    static public function get($name, $default = null) {
        $config = self::getInstance();
        $configName = reset(explode('.', $name));
        $configs = [
            $configName=>$config->getOrLoad($configName)
        ];
        return self::dot($configs, $name, true, $default);
    }

    /**
     * Получает целый конфиг по имени файла, попутно сохраняя его в кеше.
     * @param $name имя конфигурационного файла
     * @return mixed
     * @throws Exception
     */
    public function getOrLoad($name)
    {
        if(!isset($this->configs[$name])){
            $config = parse_ini_file(CONFIGS . DIRECTORY_SEPARATOR . $name . '.ini', true);
            if($config === false){
                throw new Exception('Не удалось загрузить файл настроек ' . $name);
            }
            $this->configs[$name] = $config;
        }
        return $this->configs[$name];
    }

    /**
     * Возвращает значение переданного массива используя дот-нотацию
     * @param $arr
     * @param null $path
     * @param bool $checkEmpty
     * @param null $emptyResponse
     * @return null
     */
    static public function dot(&$arr, $path = null, $checkEmpty = true, $emptyResponse = null) {
        if (!$path) user_error("Missing array path for array", E_USER_WARNING);

        $pathElements = explode(".", $path);
        $path =& $arr;

        foreach ($pathElements as $e) {
            if (!isset($path[$e])) return $emptyResponse;
            if ($checkEmpty and empty($path[$e])) return $emptyResponse;
            $path =& $path[$e];
        }

        return $path;
    }

    /**
     * переводит массив в строку ini-формата
     * @param array $arr
     * @param array $parent
     * @return string
     */
    static public function array2ini(array $arr, array $parent = [])
    {
        $out = '';
        foreach ($arr as $k => $v){
            if (is_array($v)){
                //subsection case
                //merge all the sections into one array...
                $sec = array_merge((array) $parent, (array) $k);
                //add section information to the output
                $out .= '[' . join('.', $sec) . ']' . PHP_EOL;
                //recursively traverse deeper
                $out .= self::array2ini($v, $sec);
            } else {
                //plain key->value case
                $out .= "$k=$v" . PHP_EOL;
            }
        }
        return $out;
    }

}