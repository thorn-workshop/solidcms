<?=Render::plugin(Site::get('article')->get('plugins'), ['filter'=>'head'])?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <?=Render::template('admin/head')?>
</head>

<body>
<?=Render::template('admin/header')?>
<div class="container">

    <div class="content">
        <div class="row">
            <div class="col-md-3 aside">
                <h4 class="admin-header right">Управление сайтом</h4>
            </div>
            <div class="col-md-9">
                <?php
                echo '<h4 class="admin-header left">' . Site::get('article')->get('title') . '</h4>';
                echo Site::get('article')->get('content');
                if($mycode = Render::plugin(Site::get('article')->get('plugins'))){
                    echo '<div class="mycode">'.$mycode.'</div>';
                }
                ?>
            </div>
        </div>
    </div>

</div>

</body>
</html>