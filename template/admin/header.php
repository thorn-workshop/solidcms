<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="buffer-top-small" href="/">
                <img src="/template/admin/img/logo.png" alt="<?=Config::get('system.sitename')?>" height="50"/>
            </a>
        </div>
        <div class="pull-right buffer-top-large text-smaller">
            Здравствуйте, <?=$_SESSION['user']->get('name')?> | <a href="/" target="_blank">на сайт</a> |
            <a href="/admin/logout">выйти</a>
        </div>
    </div>
</nav>