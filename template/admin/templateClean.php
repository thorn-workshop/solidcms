<?=Render::plugin(Site::get('article')->get('plugins'), ['filter'=>'head'])?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <?=Render::template('admin/head')?>
</head>

<body>

<div class="container">

    <?php
    if($mycodeBefore = Render::plugin(Site::get('article')->get('plugins'), ['filter'=>'before'])){
        echo '<div class="mycode-before">' . $mycodeBefore . '</div>';
    }

    echo '<div class="content">' . Site::get('article')->get('content') . '</div>';

    if($mycode = Render::plugin(Site::get('article')->get('plugins'))){
        echo '<div class="mycode">'.$mycode.'</div>';
    }
    ?>

</div>

</body>
</html>