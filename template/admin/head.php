<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="keywords" content="<?=Site::get('article')->get('keywords')?>">
<meta name="description" content="<?=Site::get('article')->get('description')?>">
<meta name="author" content="<?=Config::get('system.adminName')?>">
<meta http-equiv="Pragma" content="no-cache" />
<link href="/favicon.png" rel="shortcut icon" type="image/x-icon" />

<title><?=Site::get('article')->get('title')?> | <?=Config::get('system.sitename')?></title>

<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Custom styles for this template -->
<link href="/template/admin/css/style.css" rel="stylesheet">