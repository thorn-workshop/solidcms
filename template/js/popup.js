$(document).on('ready', function(){

	window.popup = {

		wrapper : $('.popup-wrapper'),
		container : $('.popup-container'),
		close : $('.popup-close'),
		cookieLifeTime : 1, //days
		mobileWidth : 600,
		mobileHeight : 500,
		runOnMobile : false,
		show : function(){
			popup.wrapper.fadeIn();
			$.cookie('popupWasShown', true, {expires: this.cookieLifeTime});
			return this;
		},
		hide : function(){
			popup.wrapper.fadeOut();
			return this;
		},
		init : function(timeout){
			if(!this.runOnMobile && this.isMobile()){
				return this;
			}
			var timeout = timeout || 0;
			this.wrapper.on('click', this.hide);
			this.container.on('click', function (e) {e.stopPropagation()});
			this.close.on('click', this.hide);

			if(!$.cookie('popupWasShown', Boolean)) {
				if (timeout) {
					window.setTimeout(popup.show, timeout);
				} else {
					this.show();
				}
			}
			return this;
		},
		reset : function(){
			$.removeCookie('popupWasShown');
			return this;
		},
		isMobile : function() {
			if(window.innerWidth <= this.mobileWidth || window.innerHeight <= this.mobileHeight) {
				return true;
			}
			return false;
		}
	};

	popup.init(5000);

});
