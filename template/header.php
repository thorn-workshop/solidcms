<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">
				<img src="/template/admin/img/logo.png" alt="<?=Config::get('system.sitename')?>" height="50"/>
			</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li <?=strpos(Site::get('article')->get('url'), 'news') !== false ? 'class="active"':''?>><a href="/news">Новости</a></li>
				<li <?=strpos(Site::get('article')->get('url'), 'doc') !== false ? 'class="active"':''?>><a href="/doc">Документация</a></li>
				<li class="dropdown<?=strpos(Site::get('article')->get('url'), 'plugin-') !== false ? ' active':''?>">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Плагины <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<?php
						foreach(Index::get('plugin-all')['children'] as $child){
							$plugin = Index::get($child);
							echo '<li><a href="/'. $plugin['url'] .'">'. $plugin['title'] .'</a></li>';
						}
						?>
						<li role="separator" class="divider"></li>
						<li class="dropdown-header">Внешние ссылки</li>
						<li><a href="http://ecomment.su" target="_blank">Скрипт комментариев Ecomments.su</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>