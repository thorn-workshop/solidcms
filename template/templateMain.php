<!DOCTYPE html>
<html lang="ru">
<head>
	<?=Render::template('head')?>
</head>

<body>

<?=Render::template('header')?>

<div class="container">

	<?php
    if($breadcrumbs = Render::plugin('breadcrumbs')){
        echo '<div class="buffer-bottom">' . $breadcrumbs . '</div>';
    }

    echo '<div class="buffer"></div>';
    if($mycodeBefore = Render::plugin(Site::get('article')->get('plugins'), ['filter'=>'before'])){
        echo '<div class="mycode-before">' . $mycodeBefore . '</div>';
    }

    echo '<div class="content">' . Site::get('article')->get('content') . '</div>';

    if(Site::get('article')->get('extra')['tags'] !== null){
        echo '<div class="buffer-bottom">'.Render::plugin('articleTags').'</div>';
    }

    if($mycode = Render::plugin(Site::get('article')->get('plugins'))){
        echo '<div class="mycode">'.$mycode.'</div>';
    }
    ?>

</div>

<?=Render::template('footer')?>

</body>
</html>
