<footer class="footer">
    <div class="container">
        <p class="buffer-top">Powered by <a href="<?=Config::get('system.siteurl')?>"><?=Config::get('system.sitename')?></a> &copy; 2016 </p>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>

<?=Render::plugin('popup')?>

