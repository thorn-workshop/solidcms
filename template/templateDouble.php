<!DOCTYPE html>
<html lang="ru">
<head>
	<?=Render::template('head')?>
</head>

<body>

<?=Render::template('header')?>

<div class="container">

    <?php
    if($breadcrumbs = Render::plugin('breadcrumbs')){
        echo '<div class="buffer-bottom">' . $breadcrumbs . '</div>';
    }
    ?>
    <div class="buffer"></div>
    <div class="row">
        <div class="col-md-9">
            <?php
            if($mycodeBefore = Render::plugin(Site::get('article')->get('plugins'), ['filter'=>'before'])){
                echo '<div class="mycode-before">' . $mycodeBefore . '</div>';
            }
            ?>
            <div class="content"><?=Site::get('article')->get('content')?></div>
            <?php
            if($mycode = Render::plugin(Site::get('article')->get('plugins'))){
                echo '<div class="mycode">'.$mycode.'</div>';
            }
            ?>
        </div>
        <div class="col-md-3 aside">
            <?php
            if($mycodeAside = Render::plugin(Site::get('article')->get('plugins'), ['filter'=>'aside'])){
                echo '<div class="mycode-aside">' . $mycodeAside . '</div>';
            }
            ?>
        </div>
    </div>
</div>

<?=Render::template('footer')?>

</body>
</html>
