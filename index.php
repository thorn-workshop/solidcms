<?php
/**
 * Description: index.php
 * Author: Геннадий Кляченков
 * Date: 04.11.2014
 * 
 */
ini_set("display_errors", 1);
ini_set("error_reporting", E_ALL & ~E_STRICT & ~E_NOTICE);

define('RPATH', (dirname(__FILE__)));
define('CORE', RPATH . DIRECTORY_SEPARATOR . 'core');
define('DATA', RPATH . DIRECTORY_SEPARATOR . 'data');
define('CONFIGS', RPATH . DIRECTORY_SEPARATOR . 'configs');
define('ARTICLES', RPATH . DIRECTORY_SEPARATOR . 'articles');
define('PLUGINS', RPATH . DIRECTORY_SEPARATOR . 'plugins');

require_once CORE . DIRECTORY_SEPARATOR . 'functions.php';
spl_autoload_register('coreAutoload');

define('RTPL', RPATH . DIRECTORY_SEPARATOR . Config::get('system.template', 'template'));

$url = parse_url($_SERVER['REQUEST_URI']);
$path = pathinfo($url['path']);

//немного красоты, чтобы избежать в будущем NOTICE о незаданном индексе массива
$path = array_merge(array(
	'dirname'=>'',
	'basename'=>'',
	'extension'=>'',
	'filename'=>''
), $path);
if(empty($path['filename'])) $path['filename'] = 'index';

// определяем идентификатор страницы
$url = substr($url['path'], 1);
if (!$url) {
	$url = 'index';
}
// открываем только адреса, похожие на статьи или разделы
$allowed_ext = array('html','xml','');
if(!in_array($path['extension'], $allowed_ext)) {
	$url = '404';
}
// ищем статью в индексе
$art = reset(Index::find('url', $url));

if(empty($art)){
	header($_SERVER['SERVER_PROTOCOL'].' 404 not found');
	$art = Index::get('404');
}

Site::showArticle($art['id']);

?>