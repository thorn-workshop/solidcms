<?php
/**
 * Description: вывод дерева статей, сгруппированных по каталогам
 * Author: Геннадий Кляченков
 * Version: 1.2
 * @var Article $article
 */

// настройки
$catalog = 'doc';
$article = Site::get('article');
$catalog = isset($catalog) ? $catalog : $article->get('id'); // идентификатор каталог, из которого нужно выводить статьи
$catalog = Index::get($catalog);

function renderArticleTree($id, $active)
{
    $children = Index::childen($id);
    $html = '';
    if(count($children)) {
        $html.= '<ul class="nav nav-list tree">';
        foreach ($children as $art) {
            $html.= '<li class="'. ($art['id'] == $active ? 'active' : '') .'">';

            $currentLink = '<a href="/'. $art['url'] .'">'. $art['title'] .'</a>';
            $currentLink = $art['catalog'] ? '<label class="tree-toggler nav-header">' . $currentLink . '</label>' : $currentLink;
            $html.= $currentLink;

            if ($art['catalog']) {
                $html.= renderArticleTree($art['id'], $active);
            }
            $html.= '</li>';
        }
        $html.= '</ul>';
    }
    return $html;
}

if($catalog['catalog']){
    echo '<div class="articles-tree well">';
    echo renderArticleTree($catalog['id'], $article->get('id'));
    echo '</div>';
}