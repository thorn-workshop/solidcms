<?php
/**
 * Created by PhpStorm.
 * User: KlyachenkovG
 * Date: 07.07.2016
 * Time: 10:29
 */

$login = Input::get('login');
$password = Input::get('password');

if($login && $password) {
    try {
        $user = new User($login);
        if ($user->auth($password)) {
            $_SESSION['user'] = $user;
            header('Location: /admin/articles');
        } else {
            throw new Exception('Неверный пароль пользователя');
        }

    } catch (Exception $e) {
        Render::share('loginErrors', [$e->getMessage()]);
    }
}
