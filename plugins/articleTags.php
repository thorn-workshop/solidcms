<?php
/**
 * Description:
 * Author: Геннадий Кляченков
 * Version: 1.0
 * Date: 17.06.2016
 * @var Article $article
 */
$article = Site::get('article');
if(isset($article->get('extra')['tags']) && is_array($article->get('extra')['tags'])) {
    $parent = Index::get($article->get('parent'));
    echo '<div class="well">';
    echo '<span class="glyphicon glyphicon-tags" aria-hidden="true"></span> Теги статьи:';

    foreach ($article->get('extra')['tags'] as $tag){
        echo '<a href="' . $parent['url'] . '?tag=' . $tag . '" class="label label-success buffer-left-small">' . $tag . '</a>';
    }

    echo '</div>';

}
