<?php
/**
 * Description: Рендерим информационные сообщения в html
 * Author: Геннадий Кляченков
 * Date: 22.03.2016*
 */
$str = '';

if (!empty($err)) {
	$str .= '<div class="alert alert-danger alert-dismissible" role="alert">';
	$str .= '<button type="button" class="close" data-dismiss="alert">&times;</button>';
	foreach ($err as $err) {
		$str .= $err . '<br />';
	}
	$str .= '</div>';
}

if (!empty($info)) {
	$str .= '<div class="alert alert-success alert-dismissible" role="alert">';
	$str .= '<button type="button" class="close" data-dismiss="alert">&times;</button>';
	foreach ($info as $info) {
		$str .= $info . '<br />';
	}
	$str .= '</div>';
}

echo $str;
?> 