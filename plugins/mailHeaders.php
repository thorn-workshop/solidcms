<?php
/**
 * Description: Почта
 * Author: Геннадий Кляченков
 * Date: 05.11.2014
 */

if(!function_exists(base64)) {
	function base64($str, $codepage = 'UTF-8')
	{
		return '=?' . $codepage . '?B?' . base64_encode($str) . '?=';
	}
}
$form = $params ? $params : array();

$headers = "From: ".base64($senderName)." <".$senderMail.">\r\n";
$headers.= "Content-type: text/html; charset=UTF-8\r\n";
$headers.= "Mime-Version: 1.0\r\n";

echo $headers;

?> 