<?php
/**
 * Description:
 * Author: Геннадий Кляченков
 * Version: 1.4
 */

// настройки
$article = Site::get('article');
$app = isset($app) ? $app : 6; // количество статей для вывода на одной страние
$catalog = isset($catalog) ? $catalog : $article->get('id'); // идентификатор каталог, из которого нужно выводить статьи

// Собираем вложенные статьи для текущего каталога
$articles = Index::lists($catalog);

// Сортируем по дате публикации
uasort($articles, function($a, $b){
	return strtotime($a['pubdate']) > strtotime($b['pubdate']) ? -1 : 1;
});

// Отфильтруем по тегам, если указано
if(Input::get('tag')) {
    $articles = array_filter($articles, function ($art){
        if(isset($art['extra']['tags']) && is_array($art['extra']['tags'])) {
            return array_search(Input::get('tag'), $art['extra']['tags']) !== false;
        }
        return false;
    });
}


$total = count($articles);
$currentPage = Input::get('page', 1) - 1;
$offset = $currentPage * $app;
// выводим только положенное на одну страницу количество статей
$articles = array_slice($articles, $offset, $app);

foreach ($articles as $art):
    $pubdate = strtotime($art['pubdate']);
    ?>

    <div class="article-block well">

        <h5 class="article-title">
            <span class="article-date label label-primary">
                <?=date('j ', $pubdate) . sayMonth(date('m', $pubdate), 'genitive'). date(' Yг.', $pubdate)?>
            </span>
            <a href="/<?=$art['url']?>" class="buffer-left-small"><?=$art['title']?></a>
            <?php
                if(isset($art['extra']['tags'])){
                    foreach($art['extra']['tags'] as $key=>$value) {
                        echo '<a href="?tag=' . $value . '" class="label label-success">' . $value . '</a>';
                    }
                }
            ?>
        </h5>

        <div class="article-preview"><?=$art['preview']?></div>
    </div>

<? endforeach;
$options = array(
    'total' => $total,
    'countPerPage'=> $app,
    'query' => [] //дополнительные параметры для рендера в ссылке пагинации
);
$pagination = new Pagination($options);
echo $pagination->render();
?>
