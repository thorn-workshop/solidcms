<?php
/**
 * Description: Генератор карты сайта в формате XML.
 * Author: Геннадий Кляченков.
 * Date: 24.03.2016
 *
 * @var Article $article
 */

//замена html-сущностей на xhtml-сущности
function xmlentities($string){
    $string = htmlentities($string, ENT_QUOTES, 'UTF-8');
    $htmlentities = array( "&quot;","&amp;","&lt;","&gt;","&nbsp;","&iexcl;","&cent;","&pound;","&curren;","&yen;","&brvbar;","&sect;","&uml;","&copy;","&ordf;","&laquo;","&not;","&shy;","&reg;","&macr;","&deg;","&plusmn;","&sup2;","&sup3;","&acute;","&micro;","&para;","&middot;","&cedil;","&sup1;","&ordm;","&raquo;","&frac14;","&frac12;","&frac34;","&iquest;","&Agrave;","&Aacute;","&Acirc;","&Atilde;","&Auml;","&Aring;","&AElig;","&Ccedil;","&Egrave;","&Eacute;","&Ecirc;","&Euml;","&Igrave;","&Iacute;","&Icirc;","&Iuml;","&ETH;","&Ntilde;","&Ograve;","&Oacute;","&Ocirc;","&Otilde;","&Ouml;","&times;","&Oslash;","&Ugrave;","&Uacute;","&Ucirc;","&Uuml;","&Yacute;","&THORN;","&szlig;","&agrave;","&aacute;","&acirc;","&atilde;","&auml;","&aring;","&aelig;","&ccedil;","&egrave;","&eacute;","&ecirc;","&euml;","&igrave;","&iacute;","&icirc;","&iuml;","&eth;","&ntilde;","&ograve;","&oacute;","&ocirc;","&otilde;","&ouml;","&divide;","&oslash;","&ugrave;","&uacute;","&ucirc;","&uuml;","&yacute;","&thorn;","&yuml;","&euro;","&ndash;","&mdash;");
    $xmlentities = array("&#34;","&#38;","&#60;","&#62;","&#160;","&#161;","&#162;","&#163;","&#164;","&#165;","&#166;","&#167;","&#168;","&#169;","&#170;","&#171;","&#172;","&#173;","&#174;","&#175;","&#176;","&#177;","&#178;","&#179;","&#180;","&#181;","&#182;","&#183;","&#184;","&#185;","&#186;","&#187;","&#188;","&#189;","&#190;","&#191;","&#192;","&#193;","&#194;","&#195;","&#196;","&#197;","&#198;","&#199;","&#200;","&#201;","&#202;","&#203;","&#204;","&#205;","&#206;","&#207;","&#208;","&#209;","&#210;","&#211;","&#212;","&#213;","&#214;","&#215;","&#216;","&#217;","&#218;","&#219;","&#220;","&#221;","&#222;","&#223;","&#224;","&#225;","&#226;","&#227;","&#228;","&#229;","&#230;","&#231;","&#232;","&#233;","&#234;","&#235;","&#236;","&#237;","&#238;","&#239;","&#240;","&#241;","&#242;","&#243;","&#244;","&#245;","&#246;","&#247;","&#248;","&#249;","&#250;","&#251;","&#252;","&#253;","&#254;","&#255;","&#8364;","&#8211;","&#8212;");
    return str_replace($htmlentities,$xmlentities,$string);
}

header ("content-type: text/xml");
$xml = new SimpleXMLElement('<urlset />');
$xml->addAttribute('xmlns', xmlentities('http://www.sitemaps.org/schemas/sitemap/0.9'));

$list = Index::lists('index', 2, true);
array_push($list, Index::get('index'));

$exclude = Config::get('sitemap.exclude', []);

foreach($list as $art){

	if(!in_array($art['id'], $exclude)){

		//определяем уровень вложенности статьи для определения priority
		$parent = $art;
		$priority = 0;
		while($parent['parent'] && $parent['parent'] != 'index' && $parent['parent'] != 'root'){
			$priority ++;
			$parent = Index::get($parent['parent']);
		}
		switch($priority){
			case 0:     $priority = Config::get('sitemap.priority.first_level', 0.5); break;
			case 1:     $priority = Config::get('sitemap.priority.second_level', 0.5); break;
			default:    $priority = Config::get('sitemap.priority.default', 0.5); break;
		}

		$url = $xml->addChild('url');
		$url->addChild('loc', xmlentities('http://'.$_SERVER['SERVER_NAME'].'/'.($art['url'] == 'index' ? '' : $art['url'])));
		$url->addChild('lastmod', date('Y-m-d', strtotime($art['pubdate'])));
		$url->addChild('changefreq', xmlentities('weekly'));
		$url->addChild('priority', $priority);
	}

}
echo $xml->asXML();