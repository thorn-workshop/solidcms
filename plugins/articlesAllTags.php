<?php
/**
 * Description: облако тегов по разделу
 * Author: Klyachenkov Gennady
 * Date: 20.06.2016
 */

// настройки
$article = Site::get('article');
$currentTag = Input::get('tag', ''); //выбранный в фильтре тег
$catalog = isset($catalog) ? $catalog : $article->get('id'); // идентификатор каталог, из которого нужно выводить статьи

// Собираем вложенные статьи для текущего каталога
$articles = Index::lists($catalog, 0, true);
$result = [];

foreach($articles as $art){
    if(isset($art['extra']['tags']) && $art['extra']['tags']){
        foreach($art['extra']['tags'] as $tag){
            $result[$tag]++;
        }
    }
}
echo '<div class="well">';
echo '<span class="glyphicon glyphicon-tags" aria-hidden="true"></span> Все теги раздела:';
foreach($result as $tag=>$count) {
    echo '<a href="?tag=' . $tag . '" class="label ' . ($tag == $currentTag ? 'label-success' : 'label-default') . ' buffer-left-small">' . $tag . '</a>';
}
echo '</div>';