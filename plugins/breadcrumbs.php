<?php
/**
 * Author Klyachenkov Gennady
 * Version 1.0
 */

$homeReplace = 'Главная';
$article = Site::get('article');
$current = $article->trim();

if($current['id'] != 'index'){
    $crumbs = [];

    while($current['parent']){
        $crumbs[] = '
            <li'.($current['id'] == $article->get('id') ? ' class="active"':'').'>
                <a href="/'.$current['url'].'">' . ($current['id'] == 'index' ? $homeReplace : $current['title']) . '</a>
            </li>';
        $current = Index::get($current['parent']);
    }

    $echo = '<ul class="breadcrumb">';
    $echo.= implode('', array_reverse($crumbs));
    $echo.= '</ul>';

    echo $echo;
}
?>