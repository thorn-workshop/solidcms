<div class="row" style="margin-top: 25%; transform: translateY(-50%);">
    <div class="col-md-4 col-md-offset-4">
        <form class="well" action="" method="post">
            <div class="col-md-12 text-center buffer-bottom-small">
                <a href="/">
                    <img src="/template/admin/img/logo.png" alt="" />
                </a>
            </div>
            <div class="row form-group buffer-bottom-small">
                <label for="inputName" class="col-md-3 control-label text-right">Логин:</label>
                <div class="col-md-9">
                    <div class="input-group">
                        <input id="inputName" name="login" value="<?=Input::get('login')?>" placeholder="" class="form-control" type="text" required>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row form-group buffer-bottom-small">
                <label for="inputPassword" class="col-md-3 control-label text-right">Пароль:</label>
                <div class="col-md-9">
                    <div class="input-group">
                        <input id="inputPassword" name="password" placeholder="" class="form-control" type="password" required>
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-lock" aria-hidden="true"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group row mbn">
                <div class="col-lg-9 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">Вход</button>
                </div>
            </div>
        </form>
    </div>
    <?php if(isset($loginErrors)): ?>
    <div class="col-md-4 col-md-offset-4">
        <div class="alert alert-dismissible alert-danger" role="alert">
            <?=implode('<br/>', $loginErrors)?>
        </div>
    </div>
    <?php endif; ?>
</div>
<?php var_dump(Render::getInstance()->getAllShares());