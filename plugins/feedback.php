<?php
/**
 * Description: Форма обратной связи
 * Author: Геннадий Кляченков
 * Date: 22.03.2016
 * 
 */
$err = $info = [];
if(isset($_POST['feedbackSubmit'])){

	if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
		$err[] = 'Введен некорректный E-mail';
	}
	if(empty($_POST['name'])){
		$err[] = 'Не указано имя';
	}
	if(empty($_POST['message'])){
		$err[] = 'Не указан текст сообщения';
	}

	if(empty($err)){
		// загружаем настройки формы обратной связи
		$config = Config::get('feedback');
		//Все ок, отправляем письмо администратору и очищаем форму
		$message = '
			Имя: '.$_POST['name'].'<br/>
			Email: '.$_POST['email'].'<br/>
			Сообщение: '.$_POST['message'].'
		';

		$headers = Render::plugin('mailHeaders', [
			'senderName' => $config['senderName'],
			'senderMail' => $_POST['email']
		]);

		if(mail($config['targetMail'], $config['mailSubject'], $message, $headers)){
			$info[] = 'Ваше сообщение было отправлено.';
			$info[] = 'Мы постараемся ответить вам максимально оперативно.';
		} else {
			$err[] = 'К сожалению, произошла техническая ошибка на стороне сервера.';
			$err[] = 'Попробуйте отправить сообщений позже.';
		}

		unset(
			$_POST['name'],
			$_POST['email'],
			$_POST['message'],
			$_POST['feedbackSubmit']
		);

	} else {
		$err[] = '<b>Сообщение не было отправлено</b> Пожалуйста, заполните все обязательные поля';
	}

}

?>

<?=Render::plugin('infoMessages', ['err'=>$err, 'info'=>$info])?>

<form method="post" class="form-horizontal feedback-form">
    <h3>Форма обратной связи:</h3>
	<div class="form-group">
		<label for="input-name" class="col-md-2 control-label">Имя:</label>
		<div class="col-md-6">
			<input type="text" name="name" class="form-control" id="input-name" required placeholder="Укажите свое имя" value="<?=$_POST['name']?>">
		</div>
	</div>
	<div class="form-group">
		<label for="input-email" class="col-md-2 control-label">E-mail:</label>
		<div class="col-md-6">
			<input type="email" name="email" class="form-control" required id="input-email" placeholder="Укажите свой E-mail" value="<?=$_POST['email']?>">
		</div>
	</div>
	<div class="form-group">
		<label for="input-message" class="col-md-2 control-label">Сообщение:</label>
		<div class="col-md-6">
			<textarea name="message" class="form-control" id="input-message" required placeholder="Введите текст сообщения"><?=$_POST['message']?></textarea>
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-6 col-md-offset-2">
			<button type="submit" name="feedbackSubmit" class="btn btn-primary">Отправить сообщение</button>
		</div>
	</div>
</form>