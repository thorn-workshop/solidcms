<?php
/**
 * Description: Рандомные ссылки
 * Author: Геннадий Кляченков
 * Date: 11.11.2014
 * 
 * @var system $this
 */
 
//статьи для исключения из списков
$exclude = array(
	'404',
	'about',
	'contacts',
	'index',
	'sitemap',
	'guestbook',
	'calendar'
);

//добавляем все каталоги в список исключений
$catalogs = $this->get_articles('index', 'catalogs', false, false);
$exclude = array_merge($exclude, array_keys($catalogs));

//пересобираем индекс с учетом исключений
$clean_index = array();
foreach($this->index as $key=>$art){
	if(!in_array($key, $exclude)){
		$clean_index[$key] = $art;
	}
}

if(!empty($clean_index)){

	//выбираем 5 случайных статей
	$links_to_show = 5;
	$links_to_show = count($clean_index) > $links_to_show ? $links_to_show : count($clean_index);

	$keys = array_rand($clean_index, $links_to_show);
	$rendered_list = '';

	foreach($keys as $key){
		$art = $clean_index[$key];
		$rendered_list.= '<div class="random-link"><a href="/'.$art['url'].'" class="link-random">'.$art['title'].'</a></div>';
	}

	echo '
	     <div class="menu-random">Публикации</div>
		 <div class="content-links">
			'.$rendered_list.'
		 </div>';

}
?>